from random import randint

name = input("Hi! What is your name? ")

for val in range(1, 6):
    guess_month =  randint(1, 12)
    guess_year =  randint(1900, 2023)
    
    is_guess_correct = input(f"Guess {val} : {name} were you born in {guess_month} / {guess_year}?\nyes or no? ")

    if is_guess_correct == "yes" and val < 6:
      print("I knew it!")
      break

    elif is_guess_correct == "no" and val < 5:
      print("Drat! Lemme try again!")
      
    else:
      print("I have other things to do. Good bye.")
      break

